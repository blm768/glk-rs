use std::ffi::OsString;

use crate::types::fileusage;

/** Filter name, convert from a sequence of raw bytes of (ostensibly) Latin-1 encoding to a native
 * OS name.
 * According to the spec, this should:
 * - remove slash, backslash, angle brackets (less-than and greater-than), colon, double-quote, pipe (vertical bar), question-mark, asterisk
 * - and truncate the argument at the first period (delete the first period)
 * - if the result is the empty string, change it to the string 'null'
 * for good measure, also exclude non-ASCII and control characters.
 */
pub fn to_os_string(buf: &[u8]) -> OsString {
    let s = buf
        .iter()
        .copied()
        .take_while(|&ch| ch != b'.')
        .filter(|&ch| {
            !(ch == b'/'
                || ch == b'\\'
                || ch == b'<'
                || ch == b'>'
                || ch == b':'
                || ch == b'"'
                || ch == b'|'
                || ch == b'?'
                || ch == b'*'
                || ch >= 127
                || ch < 32)
        })
        .map(char::from)
        .collect::<String>();
    if s.len() > 0 {
        s.into()
    } else {
        "null".into()
    }
}

/** Default extension per file usage.
 */
pub fn extension(usage: fileusage) -> OsString {
    // "It should then append an appropriate suffix, depending on the usage: ".glkdata" for
    // fileusage_Data, ".glksave" for fileusage_SavedGame, ".txt" for fileusage_Transcript and
    // fileusage_InputRecord."
    match usage.get_type() {
        fileusage::Data => "glkdata",
        fileusage::SavedGame => "glksave",
        fileusage::Transcript | fileusage::InputRecord => "txt",
        _ => "glkdata",
    }
    .into()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_to_os_string() {
        assert_eq!(to_os_string(&[]), OsString::from("null"));
        assert_eq!(to_os_string(&[0]), OsString::from("null"));
        assert_eq!(
            to_os_string(&[b'n', b'a', b'm', b'e']),
            OsString::from("name")
        );
        assert_eq!(
            to_os_string(&[b'/', b'n', b'a', b'm', b'e']),
            OsString::from("name")
        );
        assert_eq!(
            to_os_string(&[b'/', b'n', b'a', b'm', b'e', b'.', b'd', b'a', b't']),
            OsString::from("name")
        );
        assert_eq!(
            to_os_string(&[b'\x1f', b'\x7f', b'\xff', b'a', b'r', b'C', b'_']),
            OsString::from("arC_")
        );
    }

    #[test]
    fn test_extension() {
        assert_eq!(extension(fileusage::Data), OsString::from("glkdata"));
        assert_eq!(extension(fileusage::SavedGame), OsString::from("glksave"));
        assert_eq!(extension(fileusage::Transcript), OsString::from("txt"));
        assert_eq!(extension(fileusage::InputRecord), OsString::from("txt"));
        assert_eq!(
            extension(fileusage(fileusage::InputRecord.0 | fileusage::TextMode.0)),
            OsString::from("txt")
        );
        assert_eq!(extension(fileusage(12345)), OsString::from("glkdata"));
    }
}
