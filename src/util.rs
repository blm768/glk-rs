use std::slice;

pub fn unhandled(name: &str) {
    println!("unhandled: {}", name);
}

/** Like slice::from_raw_parts, but handles NULL as data, assuming len is 0. */
pub unsafe fn from_raw_parts<'a, T>(data: *const T, len: usize) -> &'a [T] {
    if len == 0 {
        &[]
    } else {
        assert!(!data.is_null());
        slice::from_raw_parts(data, len)
    }
}

/** Like slice::from_raw_parts_mut, but handles NULL as data, assuming len is 0. */
pub unsafe fn from_raw_parts_mut<'a, T>(data: *mut T, len: usize) -> &'a mut [T] {
    if len == 0 {
        &mut []
    } else {
        assert!(!data.is_null());
        slice::from_raw_parts_mut(data, len)
    }
}
