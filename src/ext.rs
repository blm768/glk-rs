/*! Allow for defining Glk extensions (usable from the glulx VM) in Rust code. */
pub use glk_sys::gluniversal_t;

/** Handlers for defining Glk extensions calls.
 */
pub trait Handlers {
    /** Called to get a function prototype in gidispatch format, for unknown Glk
     * functions.
     * This must return a static reference to a NULL-terminated slice that contains
     * the prototype string, for example `Some(b"1Iu:\x00")` or `None` if the function number is
     * unknown.
     */
    fn ext_prototype(&mut self, funcnum: u32) -> Option<&'static [u8]>;

    /** Perform a Glk call. This is called with the list of arguments provided to the
     * call. Return values should be returned in the appropriate argument slots.
     * If called with an unknown function number, do nothing.
     *
     * Handling the arglist in rust is annoying at the moment and impossible to
     * do without using unsafe, due to the untagged union.
     * TODO: A macro to (semi-)automatically generate the call dispatcher from the prototype would
     * be nice.
     */
    fn ext_call(&mut self, funcnum: u32, arglist: &mut [gluniversal_t]);

    /* gidispatch defines some other functions (for example to get the name and pointer
     * to a Glk function), but glulx only uses
     *
     * - `gidispatch_prototype`
     * - `gidispatch_call`
     * - `gidispatch_count_classes`
     *
     * To define additional classes it'd be necessary to change the latter, but this
     * seems uncommon.
     */
}
