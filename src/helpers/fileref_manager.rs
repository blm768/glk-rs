use std::fs::{self};
use std::path::{Path, PathBuf};

use crate::filename;
use crate::gidispatch;
use crate::helpers::pool;
use crate::types::{filemode, fileusage, frefid_t, Null};
use crate::util::unhandled;

/** Internal representation of a file reference. */
pub struct FileRef {
    /** Glk rock for fileref. */
    pub rock: u32,
    /** GI dispatch rock for window. */
    pub girock: gidispatch::rock,
    /** File usage. */
    pub usage: fileusage,
    /** Path to file. */
    pub path: PathBuf,
}

pub type PromptFunc = dyn FnMut(fileusage, filemode) -> Option<String>;

/** Handles fileref-related Glk calls. */
pub struct FilerefManager {
    /** Object registry callbacks. */
    obj_registry: gidispatch::ObjRegistry,
    /** Base path for named files. */
    named_base: PathBuf,
    /** Pool of filerefs. */
    filerefs: pool::ObjPool<FileRef, frefid_t>,
    /** Callback for prompt */
    prompt: Box<PromptFunc>,
}

impl FilerefManager {
    /** Create a new fileref manager. */
    pub fn new(named_base: PathBuf, prompt: Box<PromptFunc>) -> Self {
        Self {
            obj_registry: gidispatch::ObjRegistry::new(),
            named_base,
            filerefs: pool::ObjPool::new(),
            prompt,
        }
    }

    /** Set object registry and register current objects. */
    pub fn set_object_registry(&mut self, obj_registry: gidispatch::ObjRegistry) {
        self.obj_registry = obj_registry;
        // Register existing filerefs
        for (id, obj) in self.filerefs.iter_mut() {
            obj.girock = obj_registry.register(id);
        }
    }

    /** External getter for filerefs. */
    pub fn get(&self, fref: frefid_t) -> (fileusage, PathBuf) {
        let fileref = self.filerefs.get(fref).unwrap();
        (fileref.usage, fileref.path.clone())
    }

    /** Generic fileref create function. */
    pub fn create(&mut self, rock: u32, usage: fileusage, path: &Path) -> frefid_t {
        println!("path: {:?}", path);
        let (ret, fileref) = self.filerefs.insert(FileRef {
            rock,
            girock: gidispatch::rock::DUMMY,
            usage,
            path: path.to_owned(),
        });
        fileref.girock = self.obj_registry.register(ret);
        ret
    }

    /* Glk implementation: */

    pub fn create_temp(&mut self, _usage: fileusage, _rock: u32) -> frefid_t {
        // TODO use tempfile crate
        // (probably NamedTempFile::new() to allow for a separate open action)
        unhandled("glk_fileref_create_temp");
        frefid_t::NULL
    }

    pub fn create_by_name(&mut self, usage: fileusage, name: &[u8], rock: u32) -> frefid_t {
        let mut path = self.named_base.clone();
        // Append filtered file name to base path, and set extension according to spec.
        path.push(filename::to_os_string(name));
        path.set_extension(filename::extension(usage));
        self.create(rock, usage, &path)
    }

    pub fn create_by_prompt(&mut self, usage: fileusage, fmode: filemode, rock: u32) -> frefid_t {
        if let Some(s) = (self.prompt)(usage, fmode) {
            let mut path = self.named_base.clone();
            path.push(s); // No need to filter user-entered names, assume they know what they're doing.
            if path.extension().is_none() {
                // Set default extension if none given.
                path.set_extension(filename::extension(usage));
            }
            self.create(rock, usage, &path)
        } else {
            return frefid_t::NULL;
        }
    }

    pub fn create_from_fileref(&mut self, usage: fileusage, fref: frefid_t, rock: u32) -> frefid_t {
        let path = self.filerefs.get(fref).unwrap().path.clone();
        self.create(rock, usage, &path)
    }

    pub fn destroy(&mut self, fref: frefid_t) {
        let fileref = self.filerefs.get(fref).unwrap();
        // Unregister fileref from object registry
        self.obj_registry.unregister(fref, fileref.girock);
        // Finally, really remove it
        self.filerefs.remove(fref);
    }

    pub fn iterate(&mut self, fref: frefid_t) -> (frefid_t, u32) {
        let fref = self.filerefs.next(fref);
        if let Some(fileref) = self.filerefs.get(fref) {
            (fref, fileref.rock)
        } else {
            (fref, 0)
        }
    }

    pub fn get_rock(&mut self, fref: frefid_t) -> u32 {
        self.filerefs.get(fref).unwrap().rock
    }

    pub fn delete_file(&mut self, fref: frefid_t) {
        let fileref = self.filerefs.get(fref).unwrap();
        if let Err(e) = fs::remove_file(&fileref.path) {
            println!("warning: deletion of {:?} failed: {:?}", &fileref.path, e);
        }
    }

    pub fn does_file_exist(&mut self, fref: frefid_t) -> u32 {
        let fileref = self.filerefs.get(fref).unwrap();
        if fs::metadata(&fileref.path).is_ok() {
            // File exists.
            1
        } else {
            // File doesn't exist or is inaccessible, it's the same to us.
            0
        }
    }

    pub fn get_di_rock(&self, fref: frefid_t) -> gidispatch::rock {
        self.filerefs.get(fref).unwrap().girock
    }
}
