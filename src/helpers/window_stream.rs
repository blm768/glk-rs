use std::cell::RefCell;
use std::rc::Weak;

use crate::gidispatch;
use crate::helpers::stream::{Stream, Style};
use crate::types::{seekmode, stream_result_t, strid_t};

/** Functions that a window needs to implement to act as window stream. */
pub trait WindowStreamFuncs {
    fn put_buffer(&mut self, buf: &[u8]) -> strid_t;
    fn put_buffer_uni(&mut self, buf: &[u32]) -> strid_t;
    fn set_style(&mut self, styl: Style) -> strid_t;
}

/** Output stream associated with a window */
pub struct WindowStream {
    /** Glk rock for stream. */
    rock: u32,
    /** GI dispatch rock for stream. */
    girock: gidispatch::rock,
    /** Weak reference to associated window */
    window: Weak<RefCell<dyn WindowStreamFuncs>>,
}
impl WindowStream {
    pub fn new(rock: u32, window: Weak<RefCell<dyn WindowStreamFuncs>>) -> Self {
        Self {
            rock,
            girock: gidispatch::rock::DUMMY,
            window,
        }
    }
}
impl Stream for WindowStream {
    fn close(&mut self) -> Option<stream_result_t> {
        // This is illegal
        None
    }
    fn get_rock(&self) -> u32 {
        self.rock
    }
    fn set_position(&mut self, _pos: i32, _mode: seekmode) {
        // Meaningless for window streams
    }
    fn get_position(&mut self) -> u32 {
        // Meaningless for window streams
        0
    }
    fn put_buffer(&mut self, buf: &[u8]) -> strid_t {
        let rc = self.window.upgrade().unwrap();
        let mut window = rc.borrow_mut();
        window.put_buffer(buf)
    }
    fn put_buffer_uni(&mut self, buf: &[u32]) -> strid_t {
        let rc = self.window.upgrade().unwrap();
        let mut window = rc.borrow_mut();
        window.put_buffer_uni(buf)
    }
    fn set_style(&mut self, styl: Style) -> strid_t {
        let rc = self.window.upgrade().unwrap();
        let mut window = rc.borrow_mut();
        window.set_style(styl)
    }
    fn get_buffer(&mut self, _buf: &mut [u8]) -> u32 {
        0
    }
    fn get_buffer_uni(&mut self, _buf: &mut [u32]) -> u32 {
        0
    }
    fn set_di_rock(&mut self, girock: gidispatch::rock) {
        self.girock = girock;
    }
    fn get_di_rock(&self) -> gidispatch::rock {
        self.girock
    }
}
