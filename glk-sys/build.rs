extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    let mut base_config = cc::Build::new();

    base_config
        .include("glk/")
        .file("glk/gi_blorb.c")
        .file("glk/gi_dispa.c")
        .file("main_wrapper.c")
        // Disable known GCC warnings that crop up in glk support files
        .flag_if_supported("-Wno-sign-compare")
        .flag_if_supported("-Wno-unused-parameter")
        .compile("libglk.a");

    let bindings = bindgen::Builder::default()
        .header("wrapper.h")
        .clang_arg("-Iglk")
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
